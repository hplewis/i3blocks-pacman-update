# i3blocks-pacman-update
shell script for i3blocks to output the number of outdated packages with thresholding colors

## Dependencies

`checkupdates`, provided by `pacman-contrib`:
```
# pacman -S pacman-contrib
```

This provides a safe way to check the number of packages in need of updates.
