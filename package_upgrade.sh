#!/usr/bin/env bash

COUNT="$(checkupdates | wc -l)"

GREEN="#00FF00"
YELLOW="#FFF700"
RED="#FF0000"

if [[ "${COUNT}" -eq "0" ]]; then
    echo "${COUNT}"
    echo "${COUNT}"
    echo "${GREEN}"
elif [[ "${COUNT}" -lt "15" ]]; then
    echo "${COUNT}"
    echo "${COUNT}"
    echo "${GREEN}"
elif [[ "${COUNT}" -lt "40" ]]; then
    echo "${COUNT}"
    echo "${COUNT}"
    echo "${YELLOW}"
else
    echo "${COUNT}"
    echo "${COUNT}"
    echo "${RED}"
fi
